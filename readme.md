## Frontend Test Project

This test is divided into *ONE* mandatory challenge and 4 optional challenges.

In order to submit the project, you MUST complete **Challenge 1**.
For every optional challenge completed, you will earn score that will be summed into your final score.

We will select candidates who gets the highest score
_(You do not need to complete all)_

### Submission
Submission can be done in TWO ways:

1. Send zipped project files to fazri.alfan@irvinsaltedegg.com
2. **EXTRA 50 POINT**: Upload it to Github/Bitbucket and email Repository link to fazri.alfan@irvinsaltedegg.com

---

## Challenge 1 (MANDATORY)
**Build the mockup website using modern Javascript framework like React (Preferable) / Vue.js / Angular**
- Please find the mockup design in `mockup` folder
- Use this API endpoint to populate data. http://s3.irvinsaltedegg.com.s3-ap-southeast-1.amazonaws.com/engineering-test/products.json

---

### OPTIONAL CHALLENGES

### Challenge 2 (Max score: 100)
Use **SASS** , **Less** or **Stylus** CSS pre-processors

### Challenge 3 (Max score: 200)
- Add unit-test with any test frameworks

### Challenge 4 (Max score: 200)
- Use Static Typing using Flow or TypeScript

### Challenge 5 (Max score: 100)
- Use Class Validator and Class Transformer

### Challenge 6 (Max score: 50)
Submit your project on Github/Bitbucket
